#pragma once

#include <inttypes.h>

#include <stdint.h>

typedef    uintptr_t                        NSPuword_t;
typedef     intptr_t                        NSPword_t;

typedef      uint8_t                        NSPbyte;


typedef      uint16_t                       NSPuint16_t;
typedef      uint32_t                       NSPuint32_t;
typedef      uint64_t                       NSPuint64_t;

typedef      int8_t                         NSPint8_t;
typedef      int16_t                        NSPint16_t;
typedef      int32_t                        NSPint32_t;
typedef      int64_t                        NSPint64_t;

typedef     NSPword_t                       NSPboolean;

#define     NSP_ASSERT(_x_)                 assert( _x_ )
#define     NSP__STRINGRY(_x_)              #_x_
#define     NSP_STRINGRY(_x_)               NSP__STRINGRY(_x_)

typedef     struct nsp_slot_table           NSPSlotTable;
typedef     struct nsp_symbol               NSPSymbol;
typedef     struct nsp_compiled_method      NSPCompiledMethod;
typedef     struct nsp_lexical_context      NSPLexicalContext;
typedef     struct nsp_role_table           NSPRoleTable;
typedef     struct nsp_oop_array            NSPOopArray;
typedef     struct nsp_method_definition    NSPMethodDefinition;
typedef     struct nsp_object               NSPObject;
typedef     struct nsp_byte_array           NSPByteArray;
typedef     struct nsp_forwarded_object     NSPForwardedObject;
typedef     struct nsp_map                  NSPMap;
typedef     struct nsp_object_heap          NSPObjectHeap;


struct netspeak_image_header {
    NSPword_t           magic;
    NSPword_t           size;
    NSPword_t           next_hash;
    NSPword_t           special_objects_oop;
    NSPword_t           current_dispatch_id;
};


struct nsp_object {
    NSPword_t           header;
    NSPword_t           object_size;
    NSPword_t           payload_size;

    NSPMap             *map;
};


#define    NSP_HEADER_SIZE              (sizeof(NSPObject) - sizeof(NSPMap*))

#define    NSP_HEADER_SIZE_WORDS        (NSP_HEADER_SIZE / sizeof(NSPword_t))

#define    NSP_NETSPEAK_IMAGE_MAGIC     ((NSPword_t)0xABCDEF43)

#define    NSP_METHOD_CACHE_ARITY       6


#ifdef _MSC_VER
#   define      NSP_INLINE              __inline
#else
#   define      NSP_INLINE              inline
#endif  /* def _MSC_VER */


struct nsp_map {
    NSPObject                      *base;
    NSPObject                      *flags;
    NSPObject                      *representative;
};


struct nsp_oop_array {
    NSPObject                      *base;
    NSPObject                      *elements[];
    
};


struct nsp_method_cache_entry {
    NSPMethodDefinition            *method;
    NSPSymbol                      *selector;
    NSPMap                          maps[NSP_METHOD_CACHE_ARITY];
};


struct nsp_forwarded_object {
    NSPword_t                       header;
    NSPword_t                       object_size;
    NSPword_t                       payload_size;

    NSPObject                      *target;
};


struct nsp_byte_array {
    NSPObject                      *base;
    NSPbyte                         elements[];
};


struct nsp_object_heap {
    NSPbyte                         mark_color;
    NSPbyte                        *memory_old;
    NSPbyte                        *memory_young;
    NSPbyte                        *memory_old_size;

    NSPOopArray                    *special_objects_oop;
};

#define    NSP_PINNED_MASK   0x3F
#define    NSP_PINNED_OFFSET   24

#define    NSP_MARK_MASK        1

#define    NSP_MAP_FLAG_IMMUTABLE 4


#define    NSP_TRUE            ((NSPboolean)1)
#define    NSP_FALSE           ((NSPboolean)0)


enum NSP_SPECIAL {

    NSP_SPECIAL_LOBBY     = 0,

    NSP_SPECIAL_OOP_COUNT = 35
};


NSPbyte*   nsp_ptr_succ(NSPObject *self, NSPword_t amt);


NSPword_t  nsp_object_to_small_int(NSPObject *that);

NSPword_t  nsp_object_size(NSPObject *self);

NSPObject *nsp_small_int_to_object(NSPword_t);

NSPint64_t nsp_get_tick_count(void);

NSPbyte    nsp_byte_array_at(NSPObject *self, NSPword_t an_index);
