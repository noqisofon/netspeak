#pragma once

#include "misc/defs.h"
#include "vmobjects/OOObject.h"

#define NSP_MISC_LIST_FORMAT                    \
    NSP_VM_OOOBJECT_FORMAT;                     \
    size_t             size;                    \
    NSPListElement_ref head;                    \
    NSPListElement_ref tail

typedef struct nsp_list_element NSPListElement, *NSPListElement_ref;

typedef struct nsp_list {
    NSP_VTABLE(List) * _vtable;

    NSP_MISC_LIST_FORMAT;
} NSPList, *NSPList_ref;


/*!
 *
 */
NSPList_ref nsp_list_new(void);

