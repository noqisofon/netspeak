#pragma once

#include <stdint.h>

// 
// error codes
//
enum nsp_error_code {
    NSP_ERR_SUCCESS   = 0x0,
    NSP_ERR_FAIL      = 0x1,
    NSP_ERR_NOMEM     = 0x2,
    NSP_ERR_PANIC     = 0xFFFF,
};

// 
// macro for freeing an array
//
#define NSP_FREE_ARRAY_ELEMENTS(_ary_, _count_t_)       \
    for ( int32_t i = (_count_t_)-1; i >= 0; -- i ) {   \
        NSP_gc_free( (_ary_)[i] );                      \
    }

/*
 * A String hasing inline function
 * Java-like; see http://mindprod.com/jgloss/hashcode.html
 */
static inline int64_t string_hash(const char *restrict s) {
    int64_t result = 0;

    char   *ptr    = (char *)s;
    while ( ptr[0] ) {
        result =  (31 * result + *ptr ++) % INT64_MAX;
    }

    return result;
}
