#include "memory/gc.h"

#include "vmobjects/object_formats.h"
#include "vmobjects/VMObject.h"


NSP_VMObject_ref NSP_vmobject_new(void) {
    return NSP_vmobject_new_num_fields( NSP_NUMBER_OF_OBJECT_FIELDS );
}

NSP_VMObject_ref NSP_vmobject_new_num_fields(intptr_t number_of_fields) {
    size_t object_stub_size =
        sizeof(NSP_OOObject) -
        sizeof(NSP_VMObject_ref) * NSP_NUMBER_OF_OBJECT_FIELDS;

    NSP_VMObject_ref self = (NSP_VMObject_ref)NSP_gc_allocate_object( object_stub_size + sizeof(NSP_OOObject)
                                                                      * number_of_fields );

    if ( self ) {
        self->_vtable = NSP_vmobject_vtable();

        NSP_gc_start_uninterrupttable_allocation();

        NSP_INIT(self, number_of_fields);

        NSP_gc_end_uninterrupttable_allocation();
    }

    return self;
}

static NSP_VTABLE(NSP_VMObject) nsp_vmobject_vtable;
bool nsp_vmobject_vtable_inited = false;

NSP_VTABLE(NSP_VMObject) *NSP_vmobject_vtable(void) {
    if ( !nsp_vmobject_vtable_inited ) {
    }

    return &nsp_vmobject_vtable;
}
