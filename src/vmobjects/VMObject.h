#pragma once

#include <stdbool.h>

#include "misc/String.h"

#include "vmobjects/OOObject.h"

#define NSP_VM_OBJECT_VTABLE_FORMAT                                     \
    NSP_VM_OOOBJECT_VTABLE_FORMAT;                                      \
    NSP_VMClass_ref  (*get_class)(void *);                              \
    void             (*set_class)(void *, NSP_VMClass_ref);             \
    NSP_VMSymbol_ref (*get_field_name)(void *, int64_t);                \
    int64_t          (*get_field_index)(void *, NSP_VMSymbol_ref);      \
    intptr_t         (*get_number_of_fields)(void *);                   \
    int64_t          (*get_default_number_of_fields)(void *);           \
    void             (*send)(void *, NSP_VMSymbol_ref, NSP_VMObject_ref, size_t); \
    NSP_VMObject_ref (*get_field)(void *, int64_t);                     \
    void             (*set_field)(void *, int64_t, NSP_VMObject_ref);   \
    void             (*mark_references)(void *)
    

NSP_VTABLE(NSP_VMObject) {
    NSP_VM_OBJECT_VTABLE_FORMAT;
};

#define NSP_VM_OBJECT_FORMAT                        \
    NSP_VM_OOOBJECT_FORMAT;                         \
    intptr_t         num_of_fields;                 \
    NSP_VMClass_ref  class;                         \
    NSP_VMObject_ref fields[0]

#define NSP_NUMBER_OF_OBJECT_FIELDS (NSP_OBJECT_SIZE(NSP_OOObject, NSP_VMObject) - 2)

struct nsp_vm_object {
    NSP_VTABLE(NSP_VMObject) *_vtable;

    NSP_VM_OBJECT_FORMAT;
};

/*!
 *
 */
NSP_VMObject_ref NSP_vmobject_new(void);

/*!
 *
 */
NSP_VMObject_ref NSP_vmobject_new_num_fields(intptr_t);

/*!
 *
 */
NSP_VTABLE(NSP_VMObject) *NSP_vmobject_vtable(void);
