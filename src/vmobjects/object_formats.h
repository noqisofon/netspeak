#pragma once

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>


typedef _Bool NSPbool;


typedef struct nsp_vm_ooobject   NSP_OOObject,  *NSP_OOObject_ref;

typedef struct nsp_vm_object   NSP_VMObject, *NSP_VMObject_ref;
typedef struct nsp_vm_class    NSP_VMClass,  *NSP_VMClass_ref;

typedef struct nsp_vm_symbol   NSP_VMSymbol, *NSP_VMSymbol_ref;


#define NSP_VM_OOOBJECT(_that_)  ((NSP_OOObject_ref)_that_)

#define NSP_OBJECT_SIZE(_sub_, _super_)                                 \
    (sizeof(_sub_) / sizeof(void *) - sizeof(_super_) / sizeof(void *))

#define NSP_VTABLE(_class_name_)                \
    struct _##_class_name_##_vtable_format

#define NSP_METHOD(_class_name_, _method_name_) _##_class_name_##_##_method_name_

#define NSP_SEND(_receiver_, _method_, ...)                             \
    ({ typeof(_receiver_) _receiver = (_receiver_);                     \
        (_receiver->_vtable->_method_( _receiver_, ##__VA_ARGS__ ));    \
    })

#define NSP_INIT(_receiver_, ...)                                   \
    NSP_SEND( NSP_VM_OOOBJECT(_receiver_), init, ##__VA_ARGS__ )
