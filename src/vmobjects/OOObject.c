#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "memory/gc.h"

#include "vmobjects/object_formats.h"
#include "vmobjects/OOObject.h"


void _OOObject_init(void *_self, ...) {
    NSP_OOObject_ref self = NSP_VM_OOOBJECT(_self);

    self->hash = (intptr_t)self;
}

void _OOObject_free(void *_self) {
    NSP_internal_free( _self );
}

intptr_t _OOObject_object_size(void *_self) {
    return NSP_VM_OOOBJECT(_self)->object_size;
}

static NSP_VTABLE(NSP_OOObject) _OOObject_vtable;

bool   nsp_ooobject_vtable_inited = false;

NSP_VTABLE(NSP_OOObject) *NSP_ooobject_vtable(void) {
    if ( ! nsp_ooobject_vtable_inited ) {
        _OOObject_vtable._ttable     = NULL;

        _OOObject_vtable.init        = NSP_METHOD(OOObject, init);
        _OOObject_vtable.free        = NSP_METHOD(OOObject, free);
        _OOObject_vtable.object_size = NSP_METHOD(OOObject, object_size);

        nsp_ooobject_vtable_inited = true;
    }

    return &_OOObject_vtable;
}
