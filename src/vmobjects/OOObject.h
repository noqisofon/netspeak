#pragma once

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#define NSP_VM_OOOBJECT_VTABLE_FORMAT           \
    void *_ttable;                              \
    void (*init)(void *_self, ...);             \
    void (*free)(void *_self);                  \
    intptr_t (*object_size)(void *_self)

NSP_VTABLE(NSP_OOObject) {
    NSP_VM_OOOBJECT_VTABLE_FORMAT;
};

#define NSP_VM_OOOBJECT_FORMAT                  \
    intptr_t object_size;                       \
    intptr_t gc_fields;                         \
    intptr_t hash

struct nsp_vm_ooobject {
    NSP_VTABLE(NSP_OOObject) *_vtable;

    NSP_VM_OOOBJECT_FORMAT;
};

NSP_VTABLE(NSP_OOObject) *NSP_ooobject_vtable(void);
