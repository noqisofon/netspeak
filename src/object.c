#include "netspeak/netspeak.h"


void nsp_object_zero_pin_count(NSPObject *self) {
    self->header &= ~(NSP_PINNED_MASK << NSP_PINNED_OFFSET);
}

void nsp_object_set_mark(NSPObject *self, NSPObjectHeap *a_object_heap) {
    self->header &= ~NSP_MARK_MASK;
    self->header |= a_object_heap->mark_color & NSP_MARK_MASK;
}

void nsp_object_unmark(NSPObject *self, NSPObjectHeap *a_object_heap) {
    self->header &= ~NSP_MARK_MASK;
    self->header |= ( ~a_object_heap->mark_color ) & NSP_MARK_MASK;
}

void nsp_object_set_format(NSPObject *self, NSPword_t type) {
    self->header &= ~( 3 << 30 );
    self->header |= ( type & 3 ) << 30;
}


NSPboolean nsp_object_is_immutable(NSPObject *self) {
    return ( (NSPword_t)self->map->flags & NSP_MAP_FLAG_IMMUTABLE ) != 0;
}

NSPboolean nsp_object_is_special(NSPObject *self, NSPObjectHeap *a_object_heap) {
    for ( NSPword_t i = 0; i < NSP_SPECIAL_OOP_COUNT; ++ i ) {
        NSPObject  *element = a_object_heap->special_objects_oop->elements[i];

        if ( self == element ) {

            return NSP_TRUE;
        }
    }

    return NSP_FALSE;
}

NSPword_t nsp_object_array_offset(NSPObject *self) {
    return nsp_object_size( (NSPObject *)self ) * sizeof(NSPword_t);
}

NSPbyte  *nsp_byte_array_elements(NSPByteArray *self) {
    NSPbyte    *elements = (NSPbyte *)nsp_ptr_succ( (NSPObject *)self, nsp_object_array_offset( (NSPObject *)self ) );
}

NSPbyte   nsp_byte_array_at(NSPObject *self, NSPword_t an_index) {
    return nsp_byte_array_elements( (NSPByteArray *)self )[an_index];
}
