#pragma once

#include <stddef.h>


#define NSP_PAD_BYTES(_n_) ((sizeof(void *) - ((_n_) % sizeof(void *))) % sizeof(void *))


/*!
 *
 */
void NSP_gc_start_uninterrupttable_allocation(void);

/*!
 *
 */
void NSP_gc_end_uninterrupttable_allocation(void);

/*!
 *
 */
void *NSP_gc_allocate(size_t size);

/*!
 *
 */
void *NSP_gc_allocate_object(size_t size);

/*!
 *
 */
void *NSP_gc_allocate_string(const char *restrict str);

/*!
 *
 */
void  NSP_gc_free(void *ptr);

/*!
 *
 */
void *NSP_internal_allocate(size_t size);

/*!
 *
 */
void NSP_internal_free(void *ptr);
