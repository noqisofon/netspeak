#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "memory/gc.h"
#include "misc/Hashmap.h"

#include  "vmobjects/object_formats.h"
#include  "vmobjects/OOObject.h"


typedef struct free_list_entry {
    struct free_list_entry *next;
    size_t                  size;
} FreeListEntry;

void          *object_space      = NULL;

intptr_t       OBJECT_SPACE_SIZE = 1048576;

FreeListEntry *first_free_entry  = NULL;


size_t size_of_free_heap = 0;

uint32_t num_alloc;
uint32_t spc_alloc;

void *NSP_gc_allocate(size_t size) {
    if ( size == 0 ) {

        return NULL;
    }

    if ( size < sizeof(struct free_list_entry) ) {

        return NSP_internal_allocate( size );
    }

    void *result = NULL;

    // initialize variables to search through the free_list
    FreeListEntry *entry        = first_free_entry;
    FreeListEntry *before_entry = NULL;

    // don't look for the prefect match, but for the first-fit
    while ( ! ( ( entry->size == size )
                || ( entry->next == NULL )
                || ( entry->size >= (size + sizeof(FreeListEntry)) ) ) ) {
        before_entry = entry;
        entry = entry->next;
    }

    // did we find a perfect fit?
    // if so, we simply remove this entryfrom the list
    if ( entry->size == size ) {
        if ( entry == first_free_entry ) {
            // first one fitted - adjust the 'first-entry' pointer
            first_free_entry = entry->next;
        } else {
            // simply remove the reference to the found entry
            before_entry->next = entry->next;
        }
        // entry fitted

        result = entry;
    } else {
        // did we find an entry big enough for the request and a new
        // free_entry?
        if ( entry->size >= (size + sizeof(FreeListEntry)) ) {
            // save data from found entry
            size_t         old_entry_size = entry->size;
            FreeListEntry *old_next       = entry->next;

            result = entry;

            // create new entry and assign data
            FreeListEntry *replace_entry = (FreeListEntry *)((intptr_t)entry + size);

            replace_entry->size = old_entry_size - size;
            replace_entry->next = old_next;

            if ( entry == first_free_entry ) {
                first_free_entry   = replace_entry;
            } else {
                before_entry->next = replace_entry;
            }
            
        } else {
            // no space was left
            // running the GC here will most certainly result in data loss!
            
        }
    }

    if ( !result ) {
        NSP_debug_error( "Failed to allocate %ld bytes. Panic.\n", size );

        NSP_universe_exit( -1 );
    }
    memset( result, 0, size );

    // update the available size
    size_of_free_heap -= size;

    return result;
}

void *NSP_gc_allocate_object(size_t size) {
    size_t  aligned_size = size + NSP_PAD_BYTES(size);
    void   *object       = NSP_gc_allocate( aligned_size );

    if ( object ) {
        NSP_VM_OOOBJECT(object)->object_size = aligned_size;
    }

    num_alloc ++;
    spc_alloc += aligned_size;

    return object;
}

void *NSP_gc_allocate_string(const char *restrict str) {
}

void  NSP_gc_free(void *ptr) {
    if ( (    ptr <  (void *)object_space )
         || ( ptr >= (void *)((intptr_t)object_space - OBJECT_SPACE_SIZE))) {
        NSP_internal_free( ptr );
    }
}

void *NSP_internal_allocate(size_t size) {
    if ( size == 0 ) {

        return NULL;
    }

    void *result = malloc( size );
    if ( !result ) {
        NSP_debug_error( "Failed to allocate %ld bytes. Panic.\n", size );

        NSP_universe_exit( -1 );
    }

    memset( result, 0, size );

    return result;
}

void NSP_internal_free(void *ptr) {
    free( ptr );
}
