#include "vmobjects/object_formats.h"
#include "Lexer.h"
#include "Parser.h"

#include "GenerationContexts.h"
#include "BytecodeGeneration.h"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

#include "misc/defs.h"

#include "memory/gc.h"

/***

rule class-definition {
    <identifier> <equal> <superclass>
    <instance-fields> <method>*
    [ <separator> <class-fields> <method>* ]?
    <close-term>
}

rule superclass {
    <identifier>? <open-term>
}
 */
