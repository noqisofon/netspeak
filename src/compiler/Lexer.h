#pragma once

#include <ctype.h>
#include <stdio.h>
#include <stdbool.h>

#define NSPBool  _Bool
#define NSP_BUFFER_SIZE BUFSIZ

typedef enum nsp_token_kind {
    NSP_TOKEN_NONE,

    NSP_TOKEN_INTEGER,
    NSP_TOKEN_DOUBLE,
    NSP_TOKEN_NOT,

    NSP_TOKEN_AND,
    NSP_TOKEN_OR,

    NSP_TOKEN_STAR,
    NSP_TOKEN_DIV,
    NSP_TOKEN_MOD,
    NSP_TOKEN_PLUS,
    NSP_TOKEN_MINUS,

    NSP_TOKEN_EQUAL,
    NSP_TOKEN_MORE,
    NSP_TOKEN_LESS,

    NSP_TOKEN_COMMA,

    NSP_TOKEN_AT,
    NSP_TOKEN_PER,

    NSP_TOKEN_OPEN_BLOCK,
    NSP_TOKEN_CLOSE_BLOCK,

    NSP_TOKEN_COLON,

    NSP_TOKEN_PERIOD,
    NSP_TOKEN_EXIT,
    NSP_TOKEN_ASSIGN,

    NSP_TOKEN_OPEN_TERM,
    NSP_TOKEN_CLOSE_TERM,

    NSP_TOKEN_POUND,

    NSP_TOKEN_PRIMITIVE,
    NSP_TOKEN_SEPARATOR,
    NSP_TOKEN_ST_STRING,

    NSP_TOKEN_IDENTIFIER,

    NSP_TOKEN_KEYWORD,

    NSP_TOKEN_KEYWORD_SEQUENCE,

    NSP_TOKEN_OPERATOR_SEQUENCE
    
} NSPTokenKind;

extern  const char *const nsp_system_names[];

typedef struct nsp_lexer {

    FILE        *in_file;
    const char  *filename;

    int32_t      line_number;

    NSPTokenKind token_kind;
    char         token_ch;
    char         text[NSP_BUFFER_SIZE];

    NSPBool      peek_done;
    NSPTokenKind next_token_kind;
    char         next_token_ch;
    char         next_text[NSP_BUFFER_SIZE];

    char         buffer[NSP_BUFFER_SIZE];
    int32_t      buffer_position;
    
} NSPLexer;

/*!
 *
 */
NSPLexer *NSP_lexer_create(const FILE *fp, const char *filename);

/*!
 *
 */
NSPLexer *NSP_lexer_from_string(const char *clob);

/*!
 *
 */
NSPTokenKind NSP_lexer_get_token_kind(NSPLexer *self);

/*!
 *
 */
void NSP_lexer_peek(NSPLexer *self);

/*!
 *
 */
void NSP_lexer_peek_if_necessary(NSPLexer *self);


#define NSP_OPERATORP(_ch_)                                             \
    ( (_ch_) == '~'  || (_ch_) == '&' || (_ch_) == '|' || (_ch_) == '*' || (_ch_) == '/' \
      (_ch_) == '\\' || (_ch_) == '+' || (_ch_) == '=' || (_ch_) == '>' || (_ch_) == '<' \
      (_ch_) == ','  || (_ch_) == '@' || (_ch_) == '%' || (_ch_) == '-' )
