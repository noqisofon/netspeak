#pragma once

#include "vmobjects/VMArray.h"
#include "vmobjects/VMSymbol.h"

#include "misc/List.h"

#include <stdbool.h>

typedef struct nsp_class_generation_context {
    NSP_VMSymbol_ref    name;
    NSP_VMSymbol_ref    super_name;
    bool                class_side;
    NSPList_ref         instance_fields;
    NSPList_ref         instance_methods;
    NSPList_ref         class_fields;
    NSPList_ref         class_methods;
} NSPClassGenerationContext;

#define NSP_GEN_BC_SIZE 1024

// declare forward
struct nsp_method_generation_context;
typedef struct nsp_method_generation_context NSPMethodGenerationContext; 
