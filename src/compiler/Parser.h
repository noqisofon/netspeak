#pragma once

#include <stdio.h>

#include "lexer.h"

NSPLexer *NSP_parser_init(const FILE *fp, const char *filename);
