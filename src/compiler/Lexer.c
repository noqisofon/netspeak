#include "vmobjects/object_formats.h"
#include "Lexer.h"

#include <stdint.h>
#include <string.h>

#include "memory/gc.h"


const char *const nsp_system_names[] = {
    "NONE", "Integer"
};

#define NEWLINE   '\n'
#define NUL       '\0'


NSPLexer *NSP_lexer_create(const FILE *fp, const char *filename) {
    NSPLexer *lexer = NSP_internal_allocate( sizeof(NSPLexer) );

    lexer->in_file         = (FILE *)fp;
    lexer->filename        = filename;

    lexer->line_number     = 0;

    lexer->token_kind      = NSP_TOKEN_NONE;
    lexer->token_ch        = NUL;
    lexer->text[0]         = NUL;

    lexer->peek_done       = false;
    lexer->next_token_kind = NSP_TOKEN_NONE;
    lexer->next_token_ch   = NUL;
    lexer->next_text[0]    = NUL;

    lexer->buffer[0]       = NUL;
    lexer->buffer_position = 0;

    return lexer;
}

NSPLexer *NSP_lexer_from_string(const char *clob) {
    NSPLexer *lexer = NSP_lexer_create( NULL, NULL );

    strncpy( lexer->buffer, clob, NSP_BUFFER_SIZE );

    lexer->filename = "<string>";

    return lexer;
}

#define BUFFER_AT(_receiver_, _an_index_)                 ((_receiver_)->buffer[(_an_index_)])
#define BUFFER_AT_PUT(_receiver_, _an_index_, _value_)    (_receiver_)->buffer[(_an_index_)] = (_value_)

#define BUFFER_POSITION(_receiver_)                       ((_receiver_)->buffer_position)

#define BUFFER_PEEK(_receiver_)                           BUFFER_AT(_receiver_, BUFFER_POSITION(_receiver_))
#define BUFFER_EOBP(_receiver_)                           ((_receiver_)->buffer[(_receiver_)->buffer_position] == NUL)

static int32_t fill_buffer(NSPLexer *a_lexer) {
    if ( !a_lexer->in_file ) {
        // string stream

        return -1;
    }

    int32_t position = 0;
    do {
        if ( !feof( a_lexer->in_file ) ) {
            BUFFER_AT_PUT( a_lexer, position, fgetc( a_lexer->in_file ) );
        } else {
            BUFFER_AT_PUT( a_lexer, position, NEWLINE );
        }
    } while ( BUFFER_AT( a_lexer, position ) != NEWLINE );

    BUFFER_AT_PUT( a_lexer, position - 1, NUL );
    a_lexer->buffer_position      = 0;
    a_lexer->line_number         += 1;

    return position;
}

static void skip_whitespace(NSPLexer *a_lexer) {
    while ( isblank( BUFFER_PEEK(a_lexer) ) ) {
        if ( BUFFER_PEEK(a_lexer) == '\n' ) {
            a_lexer->line_number += 1;
        }

        BUFFER_POSITION(a_lexer) ++;
    }

    while ( BUFFER_EOBP(a_lexer) ) {
        if ( fill_buffer( a_lexer ) == -1 ) {

            return;
        }
    }
}

static void skip_comment() {
}

NSPTokenKind NSP_lexer_get_token_kind(NSPLexer *self) {
    if ( self->peek_done ) {
        self->peek_done = false;

        self->token_kind = self->next_token_kind;
        self->token_ch   = self->next_token_ch;

        return self->token_kind;
    }


    
    return self->token_kind;
}

void NSP_lexer_peek(NSPLexer *self) {
}

void NSP_lexer_peek_if_necessary(NSPLexer *self) {
    if ( !self->peek_done ) {
        NSP_lexer_peek( self );
    }
}
